## Guide d'utilisation

### Equipe ordre du jour :
1. utilisez l'outil de vote/sondage/questionnaire de votre choix du moment que les résultats peuvent être exporté en csv avec un résultat/vote par ligne.

   Par exemple : [FramaDate](https://framadate.org), [FramaForms](https://framaforms.org/), [LimeSurvey](https://www.limesurvey.org/), [OhMyForm](https://ohmyform.com/), ou sur WordPress [Contact Form 7](https://fr.wordpress.org/plugins/contact-form-7/) + [CFDB7](https://fr.wordpress.org/plugins/contact-form-cfdb7/)

2. incluez dans votre formulaire un champ ou chaque votant inscriera son code/jeton (et des jetons de procuration le cas échéant). En option, un 2nd champs dédié au procuration peut être utilisé.
3. transmettez l'adresse du formulaire de vote à l'équipe préparation

### Equipe préparation :
1. dans un tableur, listez les votants (avec leur nom, leur email, votant, représenté/procuration... ce qui vous arrange mais 1 par ligne)
2. exportez la liste en csv
3. importez la dans [csv-secret-ballot-generate]()
4. en plus de votre fichier d'origine vous voici avec 2 fichiers : `valid-token-list.csv`, à transmettre à l'équipe dépouillement, et `token-identity-list.csv` à utiliser au point suivant.
5. ouverez dans un tableur `token-identity-list.csv` vous y trouverez le contenu de votre fichier initial précédé d'un jeton unique par votant.
6. tranmettez par le moyen de votre choix (idéaleent un canal sécurisé) les jetons/code à chacun des votant (bonus, s'il y a des procuration, transmettez les jetons des procurrés à qui les représentera).

### Equipe dépouillement :
1. exportez les résultat du vote (quel que soit l'outil) en csv, une réponse/vote par ligne (peut inclure des procurations)
2. importez dans [csv-secret-ballot-verify]() les résultat de vote que vous venez d'exporter en csv et `valid-token-list.csv`
3. vous voici avec en quelques lignes, le nombre de votant, s'il y a eu des erreurs (avec explications associées), et les résultats des votes.
4. En bonus, vous avez le fichier de vote avec une réponse par ligne (après suppression des votes invalides et duplication des ligne avec votant + procurations pour arriver à 1 ligne = 1 voix.


## Alternatives

Sans avoir des équipes organisatrices aux quelles il est nécessaire de faire confiance, il y a : [Belenios](http://www.belenios.org/)

Je recommande également, en français : [JugementMajoritaire](https://jugementmajoritaire.net/)


Autres :
- [LiquidFeedback](https://liquidfeedback.org/) ([code source](https://www.public-software-group.org/liquid_feedback))
- [DemocracyOS, Decidim, Consul, VoteIT...](https://alternativeto.net/software/democracyos/?license=opensource)

